//
//  BaseViewController.swift
//  Stargazers
//
//  Created by MattiaCapasso on 18/08/2021.
//

import UIKit
import Foundation
import TransitionButton

class BaseViewController: CustomTransitionViewController, UIGestureRecognizerDelegate {
    // MARK: Properties
    private lazy var shadow = UIView()
    
    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: Keyboard Methods
    @objc func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlert(title: String = "Error", message: String, titleAction: String, completion: @escaping() -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: titleAction, style: .default, handler: { _ in
            completion()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
} 
