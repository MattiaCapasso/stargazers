//
//  Stargazer.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import Foundation

struct Stargazer: Codable {
    private var login: String?
    private var id: Int?
    private var nodeId: String?
    private var avatarUrl: String?
    private var gravatarId: String?
    private var url: String?
    private var htmlUrl: String?
    private var followersUrl: String?
    private var followingUrl: String?
    private var gistsUrl: String?
    private var starredUrl: String?
    private var subscriptionsUrl: String?
    private var organizationsUrl: String?
    private var reposUrl: String?
    private var eventsUrl: String?
    private var receivedEventsUrl: String?
    private var type: String?
    private var siteAdmin: Bool?
    
    init(login: String = "",
         id: Int = -1,
         nodeId: String = "",
         avatarUrl: String = "",
         gravatarId: String = "",
         url: String = "",
         htmlUrl: String = "",
         followersUrl: String = "",
         followingUrl: String = "",
         gistsUrl: String = "",
         starredUrl: String = "",
         subscriptionsUrl: String = "",
         organizationsUrl: String = "",
         reposUrl: String = "",
         eventsUrl: String = "",
         receivedEventsUrl: String = "",
         type: String = "",
         siteAdmin: Bool = false) {
        self.login = login
        self.id = id
        self.nodeId = nodeId
        self.avatarUrl = avatarUrl
        self.gravatarId = gravatarId
        self.url = url
        self.htmlUrl = htmlUrl
        self.followersUrl = followersUrl
        self.followingUrl = followingUrl
        self.gistsUrl = gistsUrl
        self.starredUrl = starredUrl
        self.subscriptionsUrl = subscriptionsUrl
        self.organizationsUrl = organizationsUrl
        self.reposUrl = reposUrl
        self.eventsUrl = eventsUrl
        self.receivedEventsUrl = receivedEventsUrl
        self.type = type
        self.siteAdmin = siteAdmin
    }
    
    enum CodingKeys: String, CodingKey {
        case login = "login"
        case id = "id"
        case nodeId = "node_id"
        case avatarUrl = "avatar_url"
        case gravatarId = "garvatar_id"
        case url = "url"
        case htmlUrl = "html_url"
        case followersUrl = "followers_url"
        case followingUrl = "following_url"
        case gistsUrl = "gists_url"
        case starredUrl = "starred_url"
        case subscriptionsUrl = "subscriptions_url"
        case organizationsUrl = "organizations_url"
        case reposUrl = "repos_url"
        case eventsUrl = "events_url"
        case receivedEventsUrl = "received_events_url"
        case type = "type"
        case siteAdmin = "site_admin"
    }
    
    var Login: String? {
        set {
            self.login = newValue
        }
        get {
            return self.login
        }
    }
    
    var ID: Int? {
        set {
            self.id = newValue
        }
        get {
            return self.id
        }
    }
    
    var NodeId: String? {
        set {
            self.nodeId = newValue
        }
        get {
            return self.nodeId
        }
    }

    var AvatarUrl: String? {
        set {
            self.avatarUrl = newValue
        }
        get {
            return self.avatarUrl
        }
    }
    
    var GravatarId: String? {
        set {
            self.gravatarId = newValue
        }
        get {
            return self.gravatarId
        }
    }
    
    var Url: String? {
        set {
            self.url = newValue
        }
        get {
            return self.url
        }
    }
    
    var HtmlUrl: String? {
        set {
            self.htmlUrl = newValue
        }
        get {
            return self.htmlUrl
        }
    }
    
    var FollowersUrl: String? {
        set {
            self.followersUrl = newValue
        }
        get {
            return self.followersUrl
        }
    }
    
    var FollowingUrl: String? {
        set {
            self.followingUrl = newValue
        }
        get {
            return self.followingUrl
        }
    }
    
    var GistsUrl: String? {
        set {
            self.gistsUrl = newValue
        }
        get {
            return self.gistsUrl
        }
    }
    
    var StarredUrl: String? {
        set {
            self.starredUrl = newValue
        }
        get {
            return self.starredUrl
        }
    }
    
    var SubscriptionsUrl: String? {
        set {
            self.subscriptionsUrl = newValue
        }
        get {
            return self.subscriptionsUrl
        }
    }
    
    var OrganizationsUrl: String? {
        set {
            self.organizationsUrl = newValue
        }
        get {
            return self.organizationsUrl
        }
    }
    
    var ReposUrl: String? {
        set {
            self.reposUrl = newValue
        }
        get {
            return self.reposUrl
        }
    }
    
    var EventsUrl: String? {
        set {
            self.eventsUrl = newValue
        }
        get {
            return self.eventsUrl
        }
    }
    
    var ReceivedEventsUrl: String? {
        set {
            self.receivedEventsUrl = newValue
        }
        get {
            return self.receivedEventsUrl
        }
    }
    
    var TypeStargazer: String? {
        set {
            self.type = newValue
        }
        get {
            return self.type
        }
    }
    
    var SiteAdmin: Bool? {
        set {
            self.siteAdmin = newValue
        }
        get {
            return self.siteAdmin
        }
    }
}
