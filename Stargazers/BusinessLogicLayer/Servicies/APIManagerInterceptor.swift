//
//  APIManagerInterceptor.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import Foundation
import Alamofire

class APIManagerInterceptor: RequestInterceptor {
    let retryLimit = 2
    let retryDelay: TimeInterval = 20
    
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        
        (request.retryCount < retryLimit) ?
            completion(.retryWithDelay(retryDelay)) : completion(.doNotRetry)
    }
}
