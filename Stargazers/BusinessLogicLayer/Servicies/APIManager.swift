//
//  APIManager.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import Foundation
import Alamofire

class APIManager {
    // MARK: Properties
    private let sessionManager: Session = {
        let configuration = URLSessionConfiguration.af.default
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60
        configuration.waitsForConnectivity = true
        return Session(configuration: configuration, interceptor: APIManagerInterceptor())
    }()
    
    internal class var shared: APIManager {
        struct Singleton {
            static let instance : APIManager = APIManager()
        }
        return Singleton.instance
    }
    
    func getStargazers(owner: String, repository: String, parameters: Parameters? = nil, success: @escaping (Bool, Error? , [Stargazer]?) -> Void) {
        
        var path = Url.stargazers
        path = path.replacingOccurrences(of: "{owner}", with: owner)
        path = path.replacingOccurrences(of: "{repository}", with: repository)
        
        let request = sessionManager.request(path, method: .get, parameters: parameters)
        request.validate(statusCode: 200..<300)
        request.responseJSON { (response) in
            switch response.result {
                case .success:
                    guard let data = response.data else { success(false, nil, nil); return }
                    let decoder = JSONDecoder()
                    let stargazers = try? decoder.decode(Array<Stargazer>.self, from: data)
                    success(true, nil, stargazers)
                case .failure(let error):
                    success(false, error, nil)
            }
        }
    }
}
