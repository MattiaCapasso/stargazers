//
//  ListRouter.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import UIKit
import Foundation

protocol ListRouter: class {
    var navigationController: UINavigationController? { get }
}

class ListRouterImplementation: ListRouter {
    weak var navigationController: UINavigationController?
}
