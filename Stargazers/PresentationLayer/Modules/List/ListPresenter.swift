//
//  ListPresenter.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import Foundation

protocol ListPresenter: class {
    func interactor(didGet stargazers: [Stargazer]?)
    func interactor(didGet error: Error?)
}

class ListPresenterImplementation: ListPresenter {
    weak var listController: ListPresenterOutput?
    
    func interactor(didGet stargazers: [Stargazer]?) {
        if let stargazers = stargazers {
            listController?.presenter(didGet: stargazers)
        }
    }
    
    func interactor(didGet error: Error?) {
        if let error = error {
            listController?.presenter(didGet: error)
        }
    }
}
