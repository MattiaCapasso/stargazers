//
//  ListInteractor.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import Foundation

protocol ListInteractor: class {
    func getStargazers(owner: String, repository: String, page: String)
}

class ListInteractorImplementation: ListInteractor {
    var presenter: ListPresenter?
    
    // MARK: API Methods
    func getStargazers(owner: String, repository: String, page: String) {
        let parameters = ["page" : page]
        APIManager.shared.getStargazers(owner: owner, repository: repository, parameters: parameters) { [self] (success, error, stargazers) in
            success ? presenter?.interactor(didGet: stargazers) : presenter?.interactor(didGet: error)
        }
    }
}
