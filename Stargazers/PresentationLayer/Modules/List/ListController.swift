//
//  ListController.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import UIKit
import Foundation

protocol ListPresenterOutput: class {
    func presenter(didGet stargazers: [Stargazer]?)
    func presenter(didGet error: Error?)
}

class ListController: BaseViewController {
    // MARK: VIP Properties
    var interactor: ListInteractor?
    weak var presenter: ListPresenter?
    var router: ListRouter?
    
    // MARK: Properties
    lazy var owner = String()
    lazy var repository = String()
    private var refreshControl = UIRefreshControl()
    @IBOutlet weak var stargazersCollection: CollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        ListConfigurator.configureModule(viewController: self)
        createRefreshControl()
        stargazersCollection.collectionDelegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getStargazers(page: "1")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

// MARK: Stargazers Collection
extension ListController {
    private func createRefreshControl() {
        scrollView.alwaysBounceVertical = true
        refreshControl.addTarget(self, action: #selector(refreshCollection), for: .valueChanged)
        scrollView.refreshControl = refreshControl
    }
    
    @objc
    func refreshCollection() {
        getStargazers(page: "1")
    }
}

// MARK: Presenter
extension ListController: ListPresenterOutput {
    private func getStargazers(page: String) {
        self.view.showAnimatedGradientSkeleton()
        interactor?.getStargazers(owner: owner, repository: repository, page: page)
    }
    
    func presenter(didGet stargazers: [Stargazer]?) {
        if let stargazers = stargazers {
            let item = stargazersCollection.sources.count-1
            stargazersCollection.sources.append(contentsOf: stargazers)
            stargazersCollection.reloadData()
            stargazersCollection.performBatchUpdates {} completion: { (done) in
                if done {
                    self.view.hideSkeleton()
                    self.refreshControl.endRefreshing()
                    self.stargazersCollection.scrollToItem(at: IndexPath(item: item, section: 0), at: .centeredVertically, animated: false)
                }
            }
        }
    }
    
    func presenter(didGet error: Error?) {
        if let error = error {
            print(error.localizedDescription)
            self.showAlert(title: "Attention", message: "No results found!", titleAction: "Continue") {
                self.view.hideSkeleton()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

// MARK: Collection Delegate
extension ListController: CollectionViewDelegate {
    func fetchData(page: String) {
        getStargazers(page: page)
    }
}
