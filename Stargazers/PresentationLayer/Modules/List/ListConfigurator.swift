//
//  ListConfigurator.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import Foundation

class ListConfigurator {
    static func configureModule(viewController: ListController) {
        let router = ListRouterImplementation()
        let interactor = ListInteractorImplementation()
        let presenter = ListPresenterImplementation()
        
        viewController.router = router
        viewController.interactor = interactor
        
        interactor.presenter = presenter
        presenter.listController = viewController as ListPresenterOutput
        router.navigationController = viewController.navigationController
    }
}
