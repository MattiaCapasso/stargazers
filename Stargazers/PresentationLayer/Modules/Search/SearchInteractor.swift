//
//  SearchInteractor.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import Foundation

protocol SearchInteractor: class {
    func checkFields(owner: TextField, repository: TextField)
}

class SearchInteractorImplementation: SearchInteractor {
    var presenter: SearchPresenter?
    
    func checkFields(owner: TextField, repository: TextField) {
        (owner.isValidText() && repository.isValidText())
            ? presenter?.interactor(fields: true, owner: owner, repository: repository)
            : presenter?.interactor(fields: false, owner: owner, repository: repository)
    }
}
