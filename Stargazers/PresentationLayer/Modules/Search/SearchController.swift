//
//  SearchController.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import UIKit
import Foundation
import TransitionButton

protocol SearchPresenterOutput: class {
    func presenter(fields areValid: Bool, owner: String, repository: String)
}

class SearchController: BaseViewController {
    // MARK: VIP Properties
    var interactor: SearchInteractor?
    weak var presenter: SearchPresenter?
    var router: SearchRouter?
    
    // MARK: Properties
    @IBOutlet weak var appVersion: UILabel!
    @IBOutlet weak var ownerField: TextField!
    @IBOutlet weak var repositoryField: TextField!
    @IBOutlet weak var searchBtn: TransitionButton!
    
    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        SearchConfigurator.configureModule(viewController: self)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        searchBtn.roundCorners(corners: .allCorners, radius: 25)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appVersion.text = UIApplication.appVersion!
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func search(_ sender: TransitionButton) {
        DispatchQueue.main.async { [self] in
            sender.startAnimation()
            interactor?.checkFields(owner: ownerField, repository: repositoryField)
        }
    }
}

// MARK: Presenter
extension SearchController: SearchPresenterOutput {
    func presenter(fields areValid: Bool, owner: String, repository: String) {
        DispatchQueue.main.async { [self] in
            self.searchBtn.stopAnimation()
            
            areValid
                ? self.router?.routeToList(with: Controller.LIST,
                                         owner: owner,
                                         repository: repository)
                : self.showAlert(message: "Fill correctly all fields",
                                 titleAction: "Continue", completion: {})
        }
    }
}
