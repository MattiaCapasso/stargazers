//
//  SearchPresenter.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import Foundation

protocol SearchPresenter: class {
    func interactor(fields areValid: Bool, owner: TextField, repository: TextField)
}

class SearchPresenterImplementation: SearchPresenter {
    weak var searchController: SearchPresenterOutput?
    
    func interactor(fields areValid: Bool, owner: TextField, repository: TextField) {
        if let owner = owner.text, let repository = repository.text {
            searchController?.presenter(fields: areValid, owner: owner, repository: repository)
        }
    }
}
