//
//  SearchConfigurator.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import Foundation

class SearchConfigurator {
    static func configureModule(viewController: SearchController) {
        let router = SearchRouterImplementation()
        let interactor = SearchInteractorImplementation()
        let presenter = SearchPresenterImplementation()
        
        viewController.router = router
        viewController.interactor = interactor
        
        interactor.presenter = presenter
        presenter.searchController = viewController as SearchPresenterOutput
        router.navigationController = viewController.navigationController
    }
}
