//
//  SearchRouter.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import UIKit

protocol SearchRouter: class {
    var navigationController: UINavigationController? { get }
    func routeToList(with id: String, owner: String, repository: String)
}

class SearchRouterImplementation: SearchRouter {
    weak var navigationController: UINavigationController?
    
    func routeToList(with id: String, owner: String, repository: String) {
        let listController: ListController?
        let storyboardList = UIStoryboard(name: Storyboard.LIST, bundle: nil)

        listController = (storyboardList.instantiateViewController(withIdentifier: id) as! ListController)
        listController?.owner = owner
        listController?.repository = repository
        navigationController?.pushViewController(listController!, animated: false)
    }
}
