//
//  StargazerCard.swift
//  Stargazers
//
//  Created by MattiaCapasso on 20/08/2021.
//

import UIKit
import Foundation

class StargazerCard: UICollectionViewCell {
    static let identifier = "StargazerCard"
    @IBOutlet weak var roundImageView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundImageView.layer.cornerRadius = self.roundImageView.frame.size.width / 2.3
        self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2.3
    }
}
