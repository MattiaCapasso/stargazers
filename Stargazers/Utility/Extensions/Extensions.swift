//
//  Extensions.swift
//  Stargazers
//
//  Created by MattiaCapasso on 18/08/2021.
//

import UIKit
import Foundation
import Kingfisher

// MARK: TextField
extension TextField {
    func isValidText() -> Bool {
        let isValid = (self.text?.count == 0) ? false : true
        return isValid
    }
}

// MARK: UIView
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

// MARK: UIImageView
extension UIImageView {
    func loadImage(url: URL) {
        DispatchQueue.main.async {
            let processor = DownsamplingImageProcessor(size: self.bounds.size)
            
            self.kf.indicatorType = .activity
            self.kf.setImage(
                with: url,
                options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(.fade(1)),
                    .cacheOriginalImage
                ])
        }
    }
}

// MARK: UIColor
extension UIColor {
    static let PERSIANROSE = UIColor(hex: "#FF1E84FF")
    
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}

// MARK: UIApplication
extension UIApplication {
    static var appVersion: String? {
        return "Version \(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String)"
    }
}
