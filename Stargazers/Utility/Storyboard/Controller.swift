//
//  Controller.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

struct Controller {
    static let SEARCH = "SearchController"
    static let LIST = "ListController"
}
