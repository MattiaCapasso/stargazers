//
//  Storyboard.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

struct Storyboard {
    static let SEARCH = "Search"
    static let LIST = "List"
}
