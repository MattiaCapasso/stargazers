//
//  Url.swift
//  Stargazers
//
//  Created by MattiaCapasso on 20/08/2021.
//

import Foundation

struct Url {
    static let basePath = "https://api.github.com/repos"
    static let stargazers = basePath+"/{owner}/{repository}/stargazers"
}
