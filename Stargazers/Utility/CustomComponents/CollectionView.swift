//
//  CollectionView.swift
//  Stargazers
//
//  Created by MattiaCapasso on 19/08/2021.
//

import UIKit
import Foundation
import SkeletonView
import Alamofire

protocol CollectionViewDelegate {
    func fetchData(page: String)
}

@IBDesignable
class CollectionView: UICollectionView {
    // MARK: Properties
    lazy var currentPage: Int = 1
    lazy var sources: [Stargazer]! = [Stargazer]()
    var collectionDelegate: CollectionViewDelegate?
    

    @IBInspectable var cardWidth: Int = 300 {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var cardHeigth: Int = 300 {
        didSet {
            setNeedsLayout()
        }
    }

    // MARK: Initialization
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        createCollectionCard()
    }
}

extension CollectionView: UICollectionViewDelegate, SkeletonCollectionViewDataSource {
    private func createCollectionCard() {
        let nib = UINib(nibName: StargazerCard.identifier, bundle: nil)
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: cardWidth, height: cardHeigth)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
    
        self.register(nib, forCellWithReuseIdentifier: StargazerCard.identifier)
        self.delegate = self
        self.dataSource = self
        self.collectionViewLayout = flowLayout
        
        self.backgroundColor = .clear
        self.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredVertically, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cardWidth, height: cardHeigth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return StargazerCard.identifier
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sources.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == sources.count - 1 {
            currentPage = currentPage + 1
            DispatchQueue.main.async {
                self.collectionDelegate?.fetchData(page: self.currentPage.description)
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StargazerCard.identifier, for: indexPath) as! StargazerCard
        
        if let sources = sources {
            if !sources.isEmpty {
                let source = sources[indexPath.row]
                let url = URL(string: source.AvatarUrl!)
                if let url = url {
                    cell.imageView.loadImage(url: url)
                } else {
                    let url = URL(string: "plus")
                    cell.imageView.loadImage(url: url!)
                }
                
                cell.name.text = source.Login!
            }
        }
        
        return cell
    }
}
