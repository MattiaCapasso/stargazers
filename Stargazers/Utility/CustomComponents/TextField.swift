//
//  TextField.swift
//  Stargazers
//
//  Created by MattiaCapasso on 20/08/2021.
//

import UIKit

class TextField: UITextField {
    var bottomLine: UIView!
    var floatingLabel: UILabel!
    var floatingLabelHeight: CGFloat = 20
    var button = UIButton(type: .custom)
    var imageView = UIImageView(frame: CGRect.zero)
    
    @IBInspectable
    var _placeholder: String?
    
    @IBInspectable
    var floatingLabelColor: UIColor = UIColor.white {
        didSet {
            self.floatingLabel.textColor = floatingLabelColor
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var activeBorderColor: UIColor = .clear
    
    @IBInspectable
    var floatingLabelBackground: UIColor = UIColor.clear.withAlphaComponent(1) {
        didSet {
            self.floatingLabel.backgroundColor = self.floatingLabelBackground
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var floatingLabelFont: UIFont = UIFont(name: "Arial", size: CGFloat(15))! {
        didSet {
            self.floatingLabel.font = self.floatingLabelFont
            self.font = self.floatingLabelFont
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var _backgroundColor: UIColor = .clear {
        didSet {
            self.layer.backgroundColor = self._backgroundColor.cgColor
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }

    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addBottomLine()
        
        // Placeholder
        self._placeholder = (self._placeholder != nil) ? self._placeholder : placeholder
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes:[NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        placeholder = self._placeholder // Make sure the placeholder is shown
        
        // Floating Label
        self.floatingLabel = UILabel(frame: CGRect.zero)
        self.addTarget(self, action: #selector(self.addFloatingLabel), for: .editingDidBegin)
        self.addTarget(self, action: #selector(self.removeFloatingLabel), for: .editingDidEnd)
    }
    
    private func addBottomLine() {
        bottomLine = UIView()
        bottomLine.frame = CGRect(x: 0.0, y: self.bounds.height - 5, width: self.bounds.width, height: 2.0)
        bottomLine.backgroundColor = UIColor.PERSIANROSE
        self.borderStyle = .none
        self.addSubview(bottomLine)
    }
    
    private func moveLabel(isUp: Bool) {
        self.setNeedsDisplay()
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: [.beginFromCurrentState, .curveEaseOut],
            animations: {
                if isUp {
                    let offset = self.floatingLabel.font.lineHeight
                    let yOrigin = CGFloat(self.floatingLabel.frame.origin.y - offset)
                    let xOrigin = CGFloat(self.floatingLabel.frame.origin.x)
                 
                    self.floatingLabel.frame = CGRect(x: xOrigin, y: yOrigin, width: self.floatingLabel.frame.width, height: self.floatingLabel.frame.height)
                } else {
                    let offset = self.floatingLabel.font.lineHeight
                    let yOrigin = CGFloat(self.frame.origin.y - offset)
                    let xOrigin = CGFloat(self.frame.origin.x)
                 
                    self.floatingLabel.frame = CGRect(x: xOrigin, y: yOrigin, width: self.frame.width, height: self.frame.height)
                }
            },
            completion: nil
        )
    }
    
    @objc func addFloatingLabel() {
        if self.text == "" {
            self.floatingLabel.textColor = floatingLabelColor
            self.floatingLabel.font = floatingLabelFont
            self.floatingLabel.text = self._placeholder
            self.floatingLabel.layer.backgroundColor = UIColor.clear.cgColor
            self.floatingLabel.translatesAutoresizingMaskIntoConstraints = false
            self.floatingLabel.clipsToBounds = true
            self.floatingLabel.textAlignment = .center
            
            self.moveLabel(isUp: true)
            self.addSubview(self.floatingLabel)
            self.layer.borderColor = self.activeBorderColor.cgColor
            
            self.floatingLabel.bottomAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
            self.placeholder = ""
        }
        
        self.bringSubviewToFront(subviews.last!)
        self.setNeedsDisplay()
    }
    
    @objc func removeFloatingLabel() {
        
        if self.text!.isEmpty {
            self.moveLabel(isUp: false)
            UIView.animate(withDuration: 0.2) { [self] in
                subviews.forEach {
                    if $0 != bottomLine {
                        $0.removeFromSuperview()
                    }
                }
                setNeedsDisplay()
            }
            self.placeholder = self._placeholder
        }
        self.layer.backgroundColor = UIColor.clear.cgColor
    }
}
